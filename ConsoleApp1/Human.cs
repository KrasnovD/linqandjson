﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Human
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public void ReadHuman()
        {
            Name = Console.ReadLine();
            Age = System.Convert.ToInt32(Console.ReadLine());
        }
        public void Writehuman()
        {
            Console.WriteLine($"Name: {Name}  Age: {Age}");
        }
    }
}
